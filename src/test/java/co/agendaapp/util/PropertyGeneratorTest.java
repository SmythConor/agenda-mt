package co.agendaapp.util;

import java.util.Random;

import org.joda.time.DateTime;

import co.agendaapp.model.User;
import co.agendaapp.model.Group;
import co.agendaapp.model.Message;
import co.agendaapp.model.Priority;
import co.agendaapp.model.Verified;
import co.agendaapp.encrypt.Encryptor;
import co.agendaapp.model.CalendarEntry;
import co.agendaapp.model.ToDoListEntry;
import co.agendaapp.encrypt.EncryptionUtils;

public class PropertyGeneratorTest {
	
	public static String generateString() {
		String str = "test-";
		int num = new Random().nextInt(100000) + 1;
		
		return str + num;
	}
	
	public static User generateUser() {
		String name = PropertyGenerator.generateString();
		String email = PropertyGenerator.generateString() + "@gmail.com";
		String password = PropertyGenerator.generateString();
		String salt = PropertyGenerator.generateString();
		
		return new User(2, name, email, password, salt, Verified.NOT, "");
	}
	
	public static User generateEncryptedUser() {
		String name = PropertyGenerator.generateString();
		String email = PropertyGenerator.generateString() + "@gmail.com";
		
		String password = "password";//PropertyGenerator.generateString();
		
		byte[] bytePassword = EncryptionUtils.stringToBytes(password);
		byte[] salt = EncryptionUtils.getSalt();
		
		String hashedPassword = Encryptor.hashPassword(bytePassword, salt);
		String saltS = EncryptionUtils.bytesToHex(salt);
		
		return new User(0, name, email, hashedPassword, saltS, Verified.NOT, "");
	}
	
	public static Group generateGroup() {
		User user = generateUser();
		String name = generateString();
		
		return new Group(0, user.getUserId(), name, "", "");
	}

	public static CalendarEntry generateCalendarEntry() {
		CalendarEntry calendarEntry = new CalendarEntry();
		
		Integer entryId = 0;
		Integer groupId = 1;
		Integer userId = 1;
		String title = generateString();
		String description = generateString();
		DateTime reminder = DateTime.now();
		DateTime entryTimestamp = DateTime.now();
		DateTime insertTimestamp = DateTime.now();
		DateTime updateTimestamp = DateTime.now();
		
		calendarEntry.setEntryId(entryId);
		calendarEntry.setGroupId(groupId);
		calendarEntry.setUserId(userId);
		calendarEntry.setTitle(title);
		calendarEntry.setDescription(description);
		calendarEntry.setRecurCount(1);
		calendarEntry.setEntryTimestamp(entryTimestamp);
		calendarEntry.setReminderTimestamp(reminder);
		calendarEntry.setInsertTimestamp(insertTimestamp);
		calendarEntry.setUpdateTimestamp(updateTimestamp);
		calendarEntry.setLastUpdateId(null);
		calendarEntry.setUpdateComment("");
		
		return calendarEntry;
	}
	
	public static ToDoListEntry generateToDoListEntry() {
		ToDoListEntry toDoListEntry = new ToDoListEntry();
		
		Integer entryId = 0;
		Integer groupId = 1;
		Integer userId = 1;
		String title = generateString();
		String description = generateString();
		Priority priority = Priority.HIGH;
		DateTime reminderTimestamp = DateTime.now();
		DateTime insertTimestamp = DateTime.now();
		DateTime updateTimestamp = DateTime.now();
		
		toDoListEntry.setEntryId(entryId);
		toDoListEntry.setGroupId(groupId);
		toDoListEntry.setUserId(userId);
		toDoListEntry.setTitle(title);
		toDoListEntry.setDescription(description);
		toDoListEntry.setPriority(priority);
		toDoListEntry.setReminderTimestamp(reminderTimestamp);
		toDoListEntry.setInsertTimestamp(insertTimestamp);
		toDoListEntry.setUpdateTimestamp(updateTimestamp);
		toDoListEntry.setLastUpdateId(null);
		toDoListEntry.setUpdateComment("");
		
		return toDoListEntry;
	}
	
	public static Message generateMessage() {
		Message message = new Message();
		Integer messageId = Math.abs(new Random().nextInt());
		Integer senderId = 1;
		Integer groupId = 1;
		String messageBody = generateString();
		DateTime sentTimestamp = DateTime.now();
		
		message.setMessageId(messageId);
		message.setSenderId(senderId);
		message.setGroupId(groupId);
		message.setMessageBody(messageBody);
		message.setSentTimestamp(sentTimestamp);
		
		return message;
	}
}