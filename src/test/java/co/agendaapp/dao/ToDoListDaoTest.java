package co.agendaapp.dao;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import co.agendaapp.model.ToDoListEntry;
import co.agendaapp.util.PropertyGeneratorTest;

@WebAppConfiguration
@ContextConfiguration("classpath:test-application-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class ToDoListDaoTest {

	@Autowired
	private ToDoListDao toDoListDao;

	/**
	 * Set up function 
	 */
	@Before
	public void setUp() {}

	/**
	 * Test getting entries for a user
	 * @throws Exception
	 */
	@Test
	public void getEntiresTest() throws Exception {
		Integer groupId = 1;

		List<ToDoListEntry> toDoListEntries = toDoListDao.getEntries(groupId);

		assertNotNull(toDoListEntries);
	}

	/**
	 * Test creating an entry
	 * @throws Exception
	 */
	@Test
	public void createEntryTest() throws Exception {
		ToDoListEntry toDoListEntry = PropertyGeneratorTest
		        .generateToDoListEntry();

		toDoListDao.createEntry(toDoListEntry);

		List<ToDoListEntry> toDoListEntries = toDoListDao
		        .getEntries(toDoListEntry.getGroupId());

		assertTrue(toDoListEntries.stream()
		        .filter(entry -> entry.getTitle()
		                .equals(toDoListEntry.getTitle()))
		        .findAny().isPresent());
	}

	/**
	 * Test updating an entry
	 * @throws Exception
	 */
	@Test
	public void updateEntryTest() throws Exception {
		ToDoListEntry toDoListEntry = PropertyGeneratorTest
		        .generateToDoListEntry();

		toDoListDao.createEntry(toDoListEntry);

		List<ToDoListEntry> createdToDoListEntries = toDoListDao
		        .getEntries(toDoListEntry.getGroupId());

		ToDoListEntry createdToDoListEntry = createdToDoListEntries.stream()
		        .filter(entry -> entry.getTitle()
		                .equals(toDoListEntry.getTitle()))
		        .findAny().orElse(null);

		String description = "New description";
		String updateComment = "update comment";

		createdToDoListEntry.setDescription(description);
		createdToDoListEntry.setUpdateComment(updateComment);

		toDoListDao.updateEntry(createdToDoListEntry);

		ToDoListEntry updatedToDoListEntry = toDoListDao
		        .getEntriesById(createdToDoListEntry.getEntryId());

		String beforeDescription = createdToDoListEntry.getDescription();
		String afterDescription = updatedToDoListEntry.getDescription();

		assertEquals(beforeDescription, afterDescription);
		assertEquals(afterDescription, description);

		String comment = updatedToDoListEntry.getUpdateComment();

		assertNotNull(comment);
		assertEquals(updateComment, comment);
	}

	/**
	 * Delete and entry test 
	 * @throws Exception
	 */
	@Test
	public void deleteEntryTest() throws Exception {
		ToDoListEntry toDoListEntry = PropertyGeneratorTest
		        .generateToDoListEntry();

		toDoListDao.createEntry(toDoListEntry);

		List<ToDoListEntry> createdToDoListEntries = toDoListDao
		        .getEntries(toDoListEntry.getGroupId());

		ToDoListEntry createdToDoListEntry = createdToDoListEntries.stream()
		        .filter(entry -> entry.getTitle()
		                .equals(toDoListEntry.getTitle()))
		        .findAny().orElse(null);

		toDoListDao.deleteEntry(createdToDoListEntry.getEntryId());

		List<ToDoListEntry> updatedToDoListEntries = toDoListDao
		        .getEntries(createdToDoListEntry.getGroupId());

		ToDoListEntry deletedToDoListEntry = updatedToDoListEntries.stream()
		        .filter(entry -> entry.getTitle()
		                .equals(createdToDoListEntry.getTitle()))
		        .findAny().orElse(null);

		assertNull(deletedToDoListEntry);
	}
}