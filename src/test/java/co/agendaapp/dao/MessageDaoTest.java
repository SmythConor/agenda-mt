package co.agendaapp.dao;

import java.util.List;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import co.agendaapp.model.Message;
import co.agendaapp.util.PropertyGeneratorTest;

import static org.junit.Assert.assertNotNull;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-application-context.xml")
public class MessageDaoTest {
	
	@Autowired
	private MessageDao messageDao;
	
	@Before
	public void setUp() {
		Message message = PropertyGeneratorTest.generateMessage();
		
		messageDao.createMessage(message);
	}
	
	@Test
	public void getMessagesTest() throws Exception {
		List<Message> messages = messageDao.getMessages(1);
		messages.stream().forEach(System.out::println);
		
		assertNotNull(messages);
	}
	
	/**
	 * Test creating a message
	 * @throws Exception
	 */
	@Test
	public void createMessageTest() throws Exception {
		Message message = PropertyGeneratorTest.generateMessage();
		System.out.println("Hello: " + message);
		
		messageDao.createMessage(message);
		
		List<Message> messages = messageDao.getMessages(message.getGroupId());
		messages.stream().forEach(System.out::println);
		
		Message createdMessage = messages
				.stream()
				.filter(entry -> entry.getMessageId().equals(message.getMessageId()))
				.findAny()
				.orElse(null);
		
		assertNotNull(createdMessage);
	}
	
	@Test
	public void updateMessageTest() throws Exception {
		Message message = PropertyGeneratorTest.generateMessage();
		
		messageDao.createMessage(message);
		
		List<Message> messages = messageDao.getMessages(message.getGroupId());
		
		Message createdMessage = messages
				.stream()
				.filter(entry -> entry.getMessageId().equals(message.getMessageId()))
				.findAny()
				.orElse(null);
		
		createdMessage.setDeliveredTimestamp(DateTime.now());
		
		messageDao.updateMessage(createdMessage);
		
		List<Message> messages1 = messageDao.getMessages(createdMessage.getGroupId());
		
		Message updatedMessage = messages1
				.stream()
				.filter(entry -> entry.getMessageId().equals(createdMessage.getMessageId()))
				.findAny()
				.orElse(null);
		
		assertNotNull(updatedMessage.getDeliveredTimestamp());
	}
}