package co.agendaapp.dao;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotEquals;

import java.util.List;

import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import co.agendaapp.model.CalendarEntry;
import co.agendaapp.util.PropertyGeneratorTest;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-application-context.xml")
public class CalendarDaoTest {

	@Autowired
	private CalendarDao calendarDao;

	CalendarEntry calendarEntry;

	/**
	 * Set up functions
	 */
	@Before
	public void setUp() {
		this.calendarEntry = PropertyGeneratorTest.generateCalendarEntry();

		calendarDao.createEntry(this.calendarEntry);
	}

	/**
	 * Test to get calendar entries for a groups
	 * @throws Exception
	 */
	@Test
	public void getEntriesTest() throws Exception {
		Integer groupId = 1;

		List<CalendarEntry> calendarEntries = calendarDao.getEntries(groupId);

		assertNotNull(calendarEntries);
	}

	/**
	 * Test for creating a calendar entry
	 * @throws Exception
	 */
	@Test
	public void createEntryTest() throws Exception {
		/* Get back all the calendar entries */
		List<CalendarEntry> calendarEntries = calendarDao
		        .getEntries(calendarEntry.getGroupId());

		assertTrue(calendarEntries.stream()
		        .filter(entry -> entry.getTitle()
		                .equals(calendarEntry.getTitle()))
		        .findAny().isPresent());
	}

	/**
	 * Test to ensure updates are successful
	 * @throws Exception
	 */
	@Test
	public void updateEntryTest() throws Exception {
		List<CalendarEntry> calendarEntries = calendarDao
		        .getEntries(calendarEntry.getGroupId());

		CalendarEntry createdEntry = calendarEntries.stream()
		        .filter(entry -> entry.getTitle()
		                .equals(calendarEntry.getTitle()))
		        .findAny().orElse(null);

		String updateText = "Updated";
		createdEntry.setDescription("New Description");
		createdEntry.setUpdateComment(updateText);

		calendarDao.updateEntry(createdEntry);

		List<CalendarEntry> updatedCalendarEntries = calendarDao
		        .getEntries(calendarEntry.getGroupId());

		CalendarEntry updatedEntry = updatedCalendarEntries.stream()
		        .filter(entry -> entry.getTitle()
		                .equals(createdEntry.getTitle()))
		        .findAny().orElse(null);

		assertNotEquals(updatedEntry.getDescription(),
		        calendarEntry.getDescription());

		assertEquals(updateText, updatedEntry.getUpdateComment());
	}

	/**
	 * Test to ensure deletion is successful
	 * @throws Exception
	 */
	@Test
	public void deleteEntryTest() throws Exception {
		List<CalendarEntry> calendarEntries = calendarDao
		        .getEntries(calendarEntry.getGroupId());

		CalendarEntry thisEntry = calendarEntries.stream()
		        .filter(entry -> entry.getTitle()
		                .equals(calendarEntry.getTitle()))
		        .findAny().orElse(null);

		calendarDao.deleteEntry(thisEntry.getEntryId());

		List<CalendarEntry> updatedCalendarEntries = calendarDao
		        .getEntries(calendarEntry.getGroupId());

		CalendarEntry deletedEntry = updatedCalendarEntries.stream()
		        .filter(entry -> entry.getTitle().equals(thisEntry.getTitle()))
		        .findAny().orElse(null);

		assertNull(deletedEntry);
	}
}