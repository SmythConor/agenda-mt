package co.agendaapp.dao;

import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import co.agendaapp.model.User;
import co.agendaapp.model.Verified;
import co.agendaapp.util.PropertyGenerator;
import co.agendaapp.util.PropertyGeneratorTest;

/**
 * Unit tests for sign up dao
 * 
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 24 Apr 2016
 * @version 0.1
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-application-context.xml")
public class SignUpDaoTest {

	@Autowired
	SignUpDao signUpDao;

	@Autowired
	UserDetailsDao userDetailsDao;

	/**
	 * Set up function
	 */
	@Before
	public void setUp() {}

	/**
	 * Test to test the sign up
	 * @throws Exception
	 */
	@Test
	public void signUpTest() throws Exception {
		/* Generate User */
		User user = PropertyGeneratorTest.generateEncryptedUser();

		/* Store values for later assertions */
		String name = user.getName();
		String email = user.getEmail();
		String password = user.getPassword();
		String salt = user.getSalt();
		Verified verified = user.getIsVerified();

		/* Sign Up user */
		signUpDao.signUp(user);

		/* Get the user back to assert */
		User user2 = userDetailsDao.getUserByEmail(email);

		String name2 = user2.getName();
		String email2 = user2.getEmail();
		Verified verified2 = user2.getIsVerified();
		String password2 = user2.getPassword();
		String salt2 = user2.getSalt();

		assertEquals(name, name2);
		assertEquals(email, email2);
		assertEquals(verified, verified2);
		assertEquals(password, password2);
		assertEquals(salt, salt2);
	}

	/**
	 * Test to test the verification
	 * @throws Exception
	 */
	@Test
	public void verifyTest() throws Exception {
		User user = PropertyGenerator.generateUser();
		String email = user.getEmail();

		signUpDao.signUp(user);

		User userAdded = userDetailsDao.getUserByEmail(email);
		int id = userAdded.getUserId();
		email = userAdded.getEmail();

		signUpDao.verify(id);

		User userVerified = userDetailsDao.getUserByEmail(email);

		Verified verified = userAdded.getIsVerified();
		Verified verified2 = userVerified.getIsVerified();

		assertNotEquals(verified, verified2);
	}
}