package co.agendaapp.dao;

import java.util.List;

import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotEquals;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import co.agendaapp.model.User;
import co.agendaapp.model.Group;
import co.agendaapp.util.PropertyGenerator;
import co.agendaapp.util.PropertyGeneratorTest;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-application-context.xml")
public class GroupDaoTest {

	@Autowired
	private GroupDao groupDao;

	@Autowired
	UserDetailsDao userDetailsDao;

	/**
	 * Set up function
	 */
	@Before
	public void setUp() {}

	/**
	 * Get group test
	 * @throws Exception
	 */
	@Test
	public void getGroupsTest() throws Exception {
		List<Group> groups = groupDao.getGroups(1);

		assertNotNull(groups);
	}

	/**
	 * Create group test
	 */
	@Test
	public void createGroupTest() {
		Group group = PropertyGenerator.generateGroup();

		groupDao.createGroup(group);

		List<Group> groups = groupDao.getGroups(group.getGroupAdminId());

		Group createdGroup = groups.stream()
		        .filter(entry -> entry.getName().equals(group.getName()))
		        .findAny().orElse(null);

		assertNotNull(createdGroup);
	}

	/**
	 * Add member test
	 * @throws Exception
	 */
	@Test
	public void addMemberTest() throws Exception {
		Group group = PropertyGenerator.generateGroup();

		groupDao.createGroup(group);

		List<Group> groups = groupDao.getGroups(group.getGroupAdminId());

		Group createdGroup = groups.stream()
		        .filter(entry -> entry.getName().equals(group.getName()))
		        .findAny().orElse(null);

		String email = "test1@gmail.com";
		User user = userDetailsDao.getUserByEmail(email);
		int userId = user.getUserId();

		groupDao.addMember(createdGroup.getGroupId(), userId);

		List<Group> updatedGroups = groupDao.getGroups(userId);

		Group addedGroup = updatedGroups.stream()
		        .filter(entry -> entry.getName().equals(group.getName()))
		        .findAny().orElse(null);

		assertEquals(addedGroup.getGroupId(), createdGroup.getGroupId());
		assertEquals(addedGroup.getGroupAdminId(), group.getGroupAdminId());
		assertNotNull(addedGroup);
	}

	/**
	 * Remove member test
	 * @throws Exception
	 */
	@Test
	public void removeMemberTest() throws Exception {
		Group group = PropertyGenerator.generateGroup();

		groupDao.createGroup(group);

		List<Group> groups = groupDao.getGroups(group.getGroupAdminId());

		Group createdGroup = groups.stream()
		        .filter(entry -> entry.getName().equals(group.getName()))
		        .findAny().orElse(null);

		String email = "test1@gmail.com";
		User user = userDetailsDao.getUserByEmail(email);

		groupDao.removeMember(createdGroup.getGroupId(), user.getUserId());

		List<Group> groups2 = groupDao.getGroups(user.getUserId());

		Group addedGroup = groups2.stream()
		        .filter(entry -> entry.getName().equals(group.getName()))
		        .findAny().orElse(null);

		assertNull(addedGroup);
	}

	/**
	 * Update member test 
	 * @throws Exception
	 */
	@Test
	public void updateNameTest() throws Exception {
		Group group = PropertyGeneratorTest.generateGroup();

		groupDao.createGroup(group);

		List<Group> groups = groupDao.getGroups(group.getGroupAdminId());

		Group createdGroup = groups.stream()
		        .filter(entry -> entry.getName().equals(group.getName()))
		        .findAny().orElse(null);

		String beforeName = createdGroup.getName();
		String name = "New Name";

		groupDao.updateName(createdGroup.getGroupId(), name);

		List<Group> updatedGroups = groupDao.getGroups(group.getGroupAdminId());

		Group updatedGroup = updatedGroups.stream()
		        .filter(entry -> entry.getName().equals(name)).findAny()
		        .orElse(null);

		assertNotEquals(beforeName, updatedGroup.getName());
	}
}
