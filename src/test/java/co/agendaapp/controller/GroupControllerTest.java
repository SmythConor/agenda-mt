package co.agendaapp.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;

import co.agendaapp.model.Group;
import co.agendaapp.model.User;
import co.agendaapp.util.PropertyGenerator;

/**
 * Unit tests for group controller
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 14 Apr 2016
 * @version 0.1
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-application-context.xml")
@WebAppConfiguration
public class GroupControllerTest {
	
	@Autowired
	WebApplicationContext context;
	
	private MockMvc mockMvc;
	
	private ObjectMapper mapper;
	
	private final String BASE_URL = "/group";
	private final String GET_GROUPS_URI = "/getGroups";
	private final String CREATE_URI = "/createGroup";
	private final String ADD_URI = "/addMember";
	
	/**
	 * Set up function
	 */
	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
		
		this.mapper = new ObjectMapper().registerModule(new JodaModule());
	}

	/**
	 * Get groups test
	 * @throws Exception
	 */
	@Test
	public void getGroupsTest() throws Exception {
		String url = BASE_URL + GET_GROUPS_URI + "/1";
		
		this.mockMvc.perform(
				get(url)
				.contentType(APPLICATION_JSON))
		.andExpect(status().isOk());
	}
	
	/**
	 * Create group test
	 * @throws Exception
	 */
	@Test
	public void createGroupTest() throws Exception {
		String url = BASE_URL + CREATE_URI;
		Group group = PropertyGenerator.generateGroup();
		
		String requestBody = this.mapper.writeValueAsString(group);
		
		this.mockMvc.perform(
				post(url)
				.contentType(APPLICATION_JSON)
				.content(requestBody))
		.andExpect(status().isCreated());
	}
	
	@Test
	public void addMemberTest() throws Exception {
		String url = BASE_URL + ADD_URI + "/1";
		String email = "test2@gmail.com";
		User user = new User();
		
		user.setEmail(email);
		
		String requestBody = this.mapper.writeValueAsString(user);
		
		this.mockMvc.perform(
				put(url)
				.contentType(APPLICATION_JSON)
				.content(requestBody))
		.andExpect(status().isAccepted());
	}
}