package co.agendaapp.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;

import org.junit.Test;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;

import co.agendaapp.model.Priority;
import co.agendaapp.model.ToDoListEntry;
import co.agendaapp.util.PropertyGeneratorTest;

/**
 * Unit tests for to do list controller
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 14 Apr 2016
 * @version 0.1
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-application-context.xml")
public class ToDoListControllerTest {
	
	@Autowired
	private WebApplicationContext webApplicationContext;
	
	private ToDoListEntry toDoListEntry;
	
	private MockMvc mockMvc;
	
	private ObjectMapper mapper;
	
	private final String BASE_URL = "/toDoList";
	private final String GET_ENTRIES_URI = "/getEntries";
	private final String CREATE_ENTRY_URI = "/createEntry";
	private final String UPDATE_ENTRY_URI = "/updateEntry";
	private final String DELETE_ENTRY_URI = "/deleteEntry";

	/**
	 * Set up function
	 */
	@Before
	public void setUp() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
		
		this.toDoListEntry = PropertyGeneratorTest.generateToDoListEntry();
		
		this.mapper = new ObjectMapper().registerModule(new JodaModule());
	}
	
	/**
	 * Test for getting to-do list entries
	 * @throws Exception
	 */
	@Test
	public void getEntriesTest() throws Exception {
		String url = BASE_URL + GET_ENTRIES_URI + "/1";
		
		mockMvc.perform(
				get(url)
				.contentType(APPLICATION_JSON))
		.andExpect(status().isOk());
	}
	
	/**
	 * Test for creating a to do list entry
	 * @throws Exception
	 */
	@Test
	public void createEntryTest() throws Exception {
		String url = BASE_URL + CREATE_ENTRY_URI;
		
		String requestBody = this.mapper.writeValueAsString(this.toDoListEntry);
		
		this.mockMvc.perform(
				post(url)
				.contentType(APPLICATION_JSON)
				.content(requestBody))
		.andExpect(status().isCreated());
	}
	
	/**
	 * Not sure how to test this
	 * Test for updating an entry
	 * @throws Exception
	 */
	@Test
	@Ignore
	public void updateEntryTest() throws Exception {
		String url = BASE_URL + UPDATE_ENTRY_URI;
		
		/* Update the entry */
		this.toDoListEntry.setPriority(Priority.MEDIUM);
		this.toDoListEntry.setUpdateComment("Changed priority to medium");
		
		String requestBody = this.mapper.writeValueAsString(this.toDoListEntry);
		
		this.mockMvc.perform(
				put(url)
				.contentType(APPLICATION_JSON)
				.content(requestBody)).andDo(print())
		.andExpect(status().isOk());
	}

	/**
	 * Test for deleting an entry
	 * @throws Exception
	 */
	@Test
	public void deleteEntryTest() throws Exception {
		String url = BASE_URL + DELETE_ENTRY_URI + "/1";
		
		this.mockMvc.perform(
				delete(url))
		.andExpect(status().isOk());
	}
}