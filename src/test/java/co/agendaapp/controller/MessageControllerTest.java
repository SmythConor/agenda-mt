package co.agendaapp.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;

import co.agendaapp.model.Message;
import co.agendaapp.util.PropertyGeneratorTest;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-application-context.xml")
public class MessageControllerTest {
	@Autowired
	private WebApplicationContext context;
	
	private MockMvc mockMvc;
	
	private ObjectMapper mapper;
	
	private final String BASE_URL = "/message";
	private final String GET_URI = "/getMessages";
	private final String POST_URI = "/createMessage";
	private final String PUT_URI = "/updateMessage";
	
	@Before
	public void setUp() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
		
		this.mapper = new ObjectMapper().registerModule(new JodaModule());
	}
	
	/**
	 * Controller test for messages
	 * @throws Exception
	 */
	@Test
	public void getMessagesTest() throws Exception {
		final String URL = BASE_URL + GET_URI + "/1";
		
		this.mockMvc.perform(
				get(URL)
				.contentType(APPLICATION_JSON))
		.andExpect(status().isOk());
	}
	
	/**
	 * Controller test to create message 
	 * @throws Exception
	 */
	@Test
	public void createMessageTest() throws Exception {
		final String URL = BASE_URL + POST_URI;
		
		Message message = PropertyGeneratorTest.generateMessage();
		
		String requestBody = this.mapper.writeValueAsString(message);
		
		this.mockMvc.perform(
				post(URL)
				.contentType(APPLICATION_JSON)
				.content(requestBody))
		.andExpect(status().isCreated());
	}
	
	/**
	 * Not sure how to test this
	 * @throws Exception
	 */
	@Test
	@Ignore
	@SuppressWarnings("unused")
	public void updateMessageTest() throws Exception {
		final String URL = BASE_URL + PUT_URI;
		
		Message message = PropertyGeneratorTest.generateMessage();
	}
}