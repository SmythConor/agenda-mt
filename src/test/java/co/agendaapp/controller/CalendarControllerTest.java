package co.agendaapp.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;


import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;

import co.agendaapp.model.CalendarEntry;
import co.agendaapp.util.PropertyGeneratorTest;

/**
 * Unit tests for Calendar controller
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 14 Apr 2016
 * @version 0.1
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-application-context.xml")
public class CalendarControllerTest {
	
	@Autowired
	private WebApplicationContext context;
	
	private MockMvc mockMvc;
	private ObjectMapper mapper;
	
	private CalendarEntry calendarEntry; 
	
	private final String BASE_URL = "/calendar";
	private final String GET_ENTRIES_URI = "/getEntries";
	private final String CREATE_ENTRY_URI = "/createEntry";
	private final String UPDATE_ENTRY_URI = "/updateEntry";
	private final String DELETE_ENTRY_URI = "/deleteEntry";

	
	/**
	 * Set up function
	 */
	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(this.context).build();
		
		/* utility entry */ 
		this.calendarEntry = PropertyGeneratorTest.generateCalendarEntry();
		
		this.mapper = new ObjectMapper().registerModule(new JodaModule());
	}

	/**
	 * Test getting entries for calendar
	 * @throws Exception
	 */
	@Test
	public void getEntriesTest() throws Exception {
		String url = BASE_URL + GET_ENTRIES_URI + "/1";
		
		this.mockMvc.perform(
				get(url)
				.contentType(APPLICATION_JSON))
		.andExpect(status().isOk());
	}
	
	/**
	 * Test creating an entry for the calendar
	 * @throws Exception
	 */
	@Test
	public void createEntryTest() throws Exception {
		String url = BASE_URL + CREATE_ENTRY_URI;
		
		String requestBody = this.mapper.writeValueAsString(this.calendarEntry);
		
		this.mockMvc.perform(
				post(url)
				.contentType(APPLICATION_JSON)
				.content(requestBody))
		.andExpect(status().isCreated());
	}
	
	/**
	 * Test updating an entry for the calendar
	 * @throws Exception
	 */
	@Test
	@Ignore
	@SuppressWarnings("unused")
	public void updateEntryTest() throws Exception {
		String url = BASE_URL + UPDATE_ENTRY_URI + "/1";
		
		CalendarEntry calendarEntry = PropertyGeneratorTest.generateCalendarEntry();
		
		/* Not sure how to test this */
		assertTrue(true);
	}
	
	@Test
	public void deleteEntryTest() throws Exception {
		String url = BASE_URL + DELETE_ENTRY_URI + "/1";
		
		this.mockMvc.perform(
				delete(url)
				.contentType(APPLICATION_JSON))
		.andExpect(status().isOk());
	}
}