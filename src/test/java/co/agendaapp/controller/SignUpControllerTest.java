package co.agendaapp.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.test.context.web.WebAppConfiguration;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;

import co.agendaapp.model.User;
import co.agendaapp.util.PropertyGenerator;

/**
 * Unit tests for sign up controller
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 14 Apr 2016
 * @version 0.1
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-application-context.xml")
@WebAppConfiguration
public class SignUpControllerTest {
	
	@Autowired
	WebApplicationContext appContext;

	private MockMvc mockMvc;
	private ObjectMapper mapper;

	private final String BASE_URL = "/signUp";

	/**
	 * Set up function
	 */
	@Before
	public void setUp() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.appContext).build();
		
		this.mapper = new ObjectMapper().registerModule(new JodaModule());
	}

	/**
	 * Sign up test
	 * @throws Exception
	 */
	@Test
	public void signUpTest() throws Exception {
		String url = BASE_URL + "/register";

		User user = PropertyGenerator.generateUser();
		
		String requestBody = this.mapper.writeValueAsString(user);

		this.mockMvc.perform(
				post(url)
				.contentType(APPLICATION_JSON)
				.content(requestBody))
		.andExpect(status().isCreated());
	}
}