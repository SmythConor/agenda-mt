package co.agendaapp.controller;

import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;

import static org.springframework.http.MediaType.APPLICATION_JSON;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;

import co.agendaapp.model.User;
import co.agendaapp.dao.SignUpDao;
import co.agendaapp.util.PropertyGeneratorTest;

/**
 * Unit tests for LogInController
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 14 Apr 2016
 * @version 0.1
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-application-context.xml")
public class LogInControllerTest {

	@Autowired
	private WebApplicationContext webApplicationContext;

	private MockMvc mockMvc;
	
	private ObjectMapper mapper;

	private final String BASE_URL = "/logIn";

	private User user;

	@Autowired
	private SignUpDao signUpDao;

	/**
	 * Set up functions
	 */
	@Before
	public void setUp() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();

		user = PropertyGeneratorTest.generateEncryptedUser();
		signUpDao.signUp(user);
		
		this.mapper = new ObjectMapper().registerModule(new JodaModule());
	}

	/**
	 * Log in test
	 * @throws Exception
	 */
	@Test
	public void logInTest() throws Exception {
		user.setPassword("password");
		
		String requestBody = this.mapper.writeValueAsString(this.user);
		
		this.mockMvc.perform(
				post(BASE_URL)
				.contentType(APPLICATION_JSON)
				.content(requestBody))
		        .andExpect(status().isOk());
	}
}