package co.agendaapp.service;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import co.agendaapp.model.User;
import co.agendaapp.util.PropertyGeneratorTest;

@Ignore
@SuppressWarnings("unused")
@WebAppConfiguration
@ContextConfiguration("classpath:test-application-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class LogInServiceTest {

	@Autowired
	private SignUpService signUpService;

	@Autowired
	private LogInService logInService;

	/**
	 * Set up function
	 */
	@Before
	public void setUp() {}

	/**
	 * Log in Service test Cannot really test at this level 
	 * @throws Exception
	 */
	@Test
	public void logInTest() throws Exception {
		assertTrue(true);
	}
}