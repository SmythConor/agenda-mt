package co.agendaapp.service;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotEquals;

import java.util.List;

import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import co.agendaapp.model.User;
import co.agendaapp.model.Group;
import co.agendaapp.dao.UserDetailsDao;
import co.agendaapp.util.PropertyGenerator;
import co.agendaapp.exception.AppLayerException;

/**
 * Unit tests for group service
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 14 Apr 2016
 * @version 0.1
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-application-context.xml")
public class GroupServiceTest {

	@Autowired
	private GroupService groupService;

	@Autowired
	private UserDetailsDao userDetailsDao;

	/**
	 * Set up function
	 */
	@Before
	public void setUp() {}

	/**
	 * Get groups for user test
	 * @throws Exception
	 */
	@Test
	public void getGroupsTest() throws Exception {
		List<Group> groups = groupService.getGroups(1);

		assertNotNull(groups);
	}

	/**
	 * Create group test
	 */
	@Test
	public void createGroupTest() {
		Group group = PropertyGenerator.generateGroup();

		groupService.createGroup(group);

		List<Group> groups = groupService.getGroups(group.getGroupAdminId());

		Group grp = groups.stream()
		        .filter(entry -> entry.getName().equals(group.getName()))
		        .findAny().orElse(null);

		assertNotNull(grp);
	}

	/**
	 * Add member test
	 * @throws Exception
	 */
	@Test
	public void addMemberTest() throws Exception {
		Group group = PropertyGenerator.generateGroup();

		groupService.createGroup(group);

		List<Group> createdGroups = groupService
		        .getGroups(group.getGroupAdminId());

		Group createdGroup = createdGroups.stream()
		        .filter(entry -> entry.getName().equals(group.getName()))
		        .findAny().orElse(null);

		String email = "test1@gmail.com";
		User user = userDetailsDao.getUserByEmail(email);
		int userId = user.getUserId();

		groupService.addMember(createdGroup.getGroupId(), user);

		List<Group> updatedGroups = groupService.getGroups(userId);

		Group grp = updatedGroups.stream()
		        .filter(entry -> entry.getName().equals(group.getName()))
		        .findAny().orElse(null);

		assertEquals(grp.getGroupId(), createdGroup.getGroupId());
		assertEquals(grp.getGroupAdminId(), group.getGroupAdminId());
		assertNotNull(grp);
	}

	/**
	 * Remove member from group test
	 * @throws Exception
	 */
	@Test
	public void removeMemberTest() throws Exception {
		Group group = PropertyGenerator.generateGroup();

		groupService.createGroup(group);

		List<Group> createdGroups = groupService
		        .getGroups(group.getGroupAdminId());

		Group createdGroup = createdGroups.stream()
		        .filter(entry -> entry.getName().equals(group.getName()))
		        .findAny().orElse(null);

		String email = "test1@gmail.com";
		User user = userDetailsDao.getUserByEmail(email);
		int userId = user.getUserId();

		groupService.addMember(createdGroup.getGroupId(), user);

		List<Group> updatedGroups = groupService.getGroups(userId);

		Group grp = updatedGroups.stream()
		        .filter(entry -> entry.getName().equals(group.getName()))
		        .findAny().orElse(null);

		assertNotNull(grp);

		groupService.removeMember(grp.getGroupId(), email);

		List<Group> updatedGroups2 = groupService.getGroups(userId);

		Group grp2 = updatedGroups2.stream()
		        .filter(entry -> entry.getName().equals(group.getName()))
		        .findAny().orElse(null);

		assertNull(grp2);
	}

	/**
	 * Update the group name test
	 * @throws Exception
	 */
	@Test
	public void updateNameTest() throws Exception {
		Group group = PropertyGenerator.generateGroup();

		groupService.createGroup(group);

		List<Group> groups = groupService.getGroups(group.getGroupAdminId());

		Group grp = groups.stream()
		        .filter(entry -> entry.getName().equals(group.getName()))
		        .findAny().orElse(null);

		String name = "updated name";

		groupService.updateName(grp.getGroupId(), name);

		List<Group> groups2 = groupService.getGroups(group.getGroupAdminId());

		Group grp2 = groups2.stream()
		        .filter(entry -> entry.getName().equals(name)).findAny()
		        .orElse(null);

		assertNotNull(grp2);

		String beforeName = grp.getName();
		String afterName = grp2.getName();

		assertNotEquals(beforeName, afterName);
		assertEquals(afterName, name);
	}

	/**
	 * Remove a member that's in the group test
	 * @throws Exception
	 */
	@Test
	public void removeMemberNotInGroupTest() throws Exception {
		Group group = PropertyGenerator.generateGroup();

		groupService.createGroup(group);

		List<Group> createdGroups = groupService
		        .getGroups(group.getGroupAdminId());

		Group createdGroup = createdGroups.stream()
		        .filter(entry -> entry.getName().equals(group.getName()))
		        .findAny().orElse(null);

		String email = "test2@gmail.com";
		boolean success = false;

		try {
			groupService.removeMember(createdGroup.getGroupId(), email);
		} catch(AppLayerException e) {
			if(e.getStatus().equals(HttpStatus.NOT_FOUND)) {
				success = true;
			}
		}

		assertTrue(success);
	}

	/**
	 * Remove a member that doesn't exist test
	 * @throws Exception
	 */
	@Test
	public void removeMemberUserNotFound() throws Exception {
		Group group = PropertyGenerator.generateGroup();

		groupService.createGroup(group);

		List<Group> createdGroups = groupService
		        .getGroups(group.getGroupAdminId());

		Group createdGroup = createdGroups.stream()
		        .filter(entry -> entry.getName().equals(group.getName()))
		        .findAny().orElse(null);

		boolean success = false;

		try {
			groupService.removeMember(createdGroup.getGroupId(), "x");
		} catch(AppLayerException e) {
			if(e.getStatus().equals(HttpStatus.NOT_FOUND)) {
				success = true;
			}
		}

		assertTrue(success);
	}
}
