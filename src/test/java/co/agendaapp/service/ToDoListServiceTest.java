package co.agendaapp.service;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotEquals;

import java.util.List;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.Before;
import org.joda.time.DateTime;
import org.junit.runner.RunWith;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import co.agendaapp.model.ToDoListEntry;
import co.agendaapp.util.PropertyGeneratorTest;
import co.agendaapp.exception.AppLayerException;

/**
 * Unit Tests for To do list service
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 14 Apr 2016
 * @version 0.1
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-application-context.xml")
public class ToDoListServiceTest {

	@Autowired
	private ToDoListService toDoListService;

	@Before
	public void setUp() {}

	/**
	 * Test Get entries for to do list
	 * @throws Exception
	 */
	@Test
	public void getEntriesTest() throws Exception {
		Integer groupId = 1;

		List<ToDoListEntry> toDoListEntries = toDoListService
		        .getEntries(groupId);

		assertNotNull(toDoListEntries);
	}

	/**
	 * Create entry test
	 * @throws Exception
	 */
	@Test
	public void createEntryTest() throws Exception {
		ToDoListEntry toDoListEntry = PropertyGeneratorTest
		        .generateToDoListEntry();

		toDoListService.createEntry(toDoListEntry);

		List<ToDoListEntry> toDoListEntries = toDoListService
		        .getEntries(toDoListEntry.getGroupId());

		ToDoListEntry createdToDoListEntry = toDoListEntries.stream()
		        .filter(entry -> entry.getTitle()
		                .equals(toDoListEntry.getTitle()))
		        .findAny().orElse(null);

		assertNotNull(createdToDoListEntry);
	}

	/**
	 * Update an entry test
	 * @throws Exception
	 */
	@Test
	public void updateEntryTest() throws Exception {
		ToDoListEntry toDoListEntry = PropertyGeneratorTest
		        .generateToDoListEntry();

		toDoListService.createEntry(toDoListEntry);

		List<ToDoListEntry> toDoListEntries = toDoListService
		        .getEntries(toDoListEntry.getGroupId());

		ToDoListEntry createdToDoListEntry = toDoListEntries.stream()
		        .filter(entry -> entry.getTitle()
		                .equals(toDoListEntry.getTitle()))
		        .findAny().orElse(null);

		String beforeDescription = createdToDoListEntry.getDescription();
		String beforeUpdateComment = createdToDoListEntry.getUpdateComment();

		String updatedDescription = "new description";
		String updatedComment = "Update";

		createdToDoListEntry.setDescription(updatedDescription);
		createdToDoListEntry.setUpdateComment(updatedComment);

		toDoListService.updateEntry(createdToDoListEntry);

		List<ToDoListEntry> updatedToDoListEntries = toDoListService
		        .getEntries(createdToDoListEntry.getGroupId());

		ToDoListEntry updatedToDoListEntry = updatedToDoListEntries.stream()
		        .filter(entry -> entry.getTitle()
		                .equals(createdToDoListEntry.getTitle()))
		        .findAny().orElse(null);

		String afterDescription = updatedToDoListEntry.getDescription();

		assertNotEquals(beforeDescription, afterDescription);

		assertNull(beforeUpdateComment);

		assertEquals(afterDescription, updatedDescription);

		assertEquals(updatedComment, updatedToDoListEntry.getUpdateComment());
	}

	/**
	 * Test deleting an entry
	 * @throws Exception
	 */
	@Test
	public void deleteEntryTest() throws Exception {
		ToDoListEntry toDoListEntry = PropertyGeneratorTest
		        .generateToDoListEntry();

		toDoListService.createEntry(toDoListEntry);

		List<ToDoListEntry> toDoListEntries = toDoListService
		        .getEntries(toDoListEntry.getGroupId());

		ToDoListEntry createdToDoListEntry = toDoListEntries.stream()
		        .filter(entry -> entry.getTitle()
		                .equals(toDoListEntry.getTitle()))
		        .findAny().orElse(null);

		toDoListService.deleteEntry(createdToDoListEntry.getEntryId());

		List<ToDoListEntry> updatedToDoListEntries = toDoListService
		        .getEntries(createdToDoListEntry.getGroupId());

		ToDoListEntry deletedToDoListEntry = updatedToDoListEntries.stream()
		        .filter(entry -> entry.getTitle()
		                .equals(createdToDoListEntry.getTitle()))
		        .findAny().orElse(null);

		assertNull(deletedToDoListEntry);
	}

	/**
	 * Stale update test
	 * @throws Exception
	 */
	@Test
	public void staleUpdateTest() throws Exception {
		ToDoListEntry toDoListEntry = PropertyGeneratorTest
		        .generateToDoListEntry();

		toDoListService.createEntry(toDoListEntry);

		List<ToDoListEntry> toDoListEntries = toDoListService
		        .getEntries(toDoListEntry.getGroupId());

		ToDoListEntry createdToDoListEntry = toDoListEntries.stream()
		        .filter(entry -> entry.getTitle()
		                .equals(toDoListEntry.getTitle()))
		        .findAny().orElse(null);

		String updatedDescription = "new description";
		String updatedComment = "Update";
		DateTime temp = createdToDoListEntry.getUpdateTimestamp();

		temp = temp.minus(1000);

		createdToDoListEntry.setUpdateTimestamp(temp);

		createdToDoListEntry.setDescription(updatedDescription);
		createdToDoListEntry.setUpdateComment(updatedComment);

		boolean success = false;

		try {
			toDoListService.updateEntry(createdToDoListEntry);
		} catch(AppLayerException e) {
			success = true;
		}

		assertTrue(success);
	}

	/**
	 * Delete entry for unauthorized user test 
	 * @throws Exception
	 */
	@Test
	@Ignore
	public void unauthorizedDeleteEntryTest() throws Exception {
		ToDoListEntry toDoListEntry = PropertyGeneratorTest
		        .generateToDoListEntry();

		toDoListService.createEntry(toDoListEntry);

		List<ToDoListEntry> toDoListEntries = toDoListService
		        .getEntries(toDoListEntry.getGroupId());

		ToDoListEntry createdToDoListEntry = toDoListEntries.stream()
		        .filter(entry -> entry.getTitle()
		                .equals(toDoListEntry.getTitle()))
		        .findAny().orElse(null);

		boolean success = false;

		try {
			toDoListService.deleteEntry(createdToDoListEntry.getEntryId());
		} catch(AppLayerException e) {
			success = true;
		}

		assertTrue(success);
	}
}
