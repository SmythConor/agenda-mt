package co.agendaapp.service;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.joda.time.DateTime;

import org.junit.Test;
import org.junit.Ignore;
import org.junit.Before;
import org.junit.runner.RunWith;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import co.agendaapp.model.CalendarEntry;
import co.agendaapp.util.PropertyGeneratorTest;
import co.agendaapp.exception.AppLayerException;

/**
 * Unit tests for calendar service
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 14 Apr 2016
 * @version 0.1
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-application-context.xml")
public class CalendarServiceTest {
	@Autowired
	private CalendarService calendarService;
	
	/**
	 * Set up functions
	 */
	@Before
	public void setUp() {}
	
	/**
	 * Test to get calendar entries for a groups
	 * @throws Exception
	 */
	@Test
	public void getEntriesTest() throws Exception {
		Integer groupId = 1;
		
		List<CalendarEntry> calendarEntries = calendarService.getEntries(groupId);
		
		assertNotNull(calendarEntries);
	}
	
	/**
	 * Test for creating an entry
	 * @throws Exception
	 */
	@Test
	public void createEntryTest() throws Exception {
		CalendarEntry calendarEntry = PropertyGeneratorTest.generateCalendarEntry();
		
		calendarService.createEntry(calendarEntry);
		
		List<CalendarEntry> calendarEntries = calendarService.getEntries(calendarEntry.getGroupId());
		
		CalendarEntry createdEntry = calendarEntries.stream()
				.filter(entry -> entry.getTitle().equals(calendarEntry.getTitle()))
				.findAny()
				.orElse(null);
		
		assertNotNull(createdEntry);
	}
	
	/**
	 * Test to ensure updates are successful
	 * @throws Exception
	 */
	@Test
	public void updateEntryTest() throws Exception {
		CalendarEntry calendarEntry = PropertyGeneratorTest.generateCalendarEntry();
		
		calendarService.createEntry(calendarEntry);
		
		List<CalendarEntry> calendarEntries = calendarService.getEntries(calendarEntry.getGroupId());
		
		CalendarEntry createdEntry = calendarEntries.stream()
				.filter(entry -> entry.getTitle().equals(calendarEntry.getTitle()))
				.findAny()
				.orElse(null);
		
		String updateDescription = "new description";
		String updateMessage = "Updated";
		
		createdEntry.setDescription(updateDescription);
		createdEntry.setUpdateComment(updateMessage);
		
		calendarService.updateEntry(createdEntry);
		
		List<CalendarEntry> updatedEntries = calendarService.getEntries(calendarEntry.getGroupId());
		
		CalendarEntry updatedEntry = updatedEntries.stream()
				.filter(entry -> entry.getTitle().equals(createdEntry.getTitle()))
				.findAny()
				.orElse(null);
		
		assertNotNull(updatedEntry);
		 
		assertEquals(updateDescription, updatedEntry.getDescription());
		assertEquals(updateMessage, updatedEntry.getUpdateComment());
	}
	
	/**
	 * Test to ensure deletion is successful
	 * @throws Exception
	 */
	@Test 
	public void deleteEntryTest() throws Exception {
		CalendarEntry calendarEntry = PropertyGeneratorTest.generateCalendarEntry();
		
		calendarService.createEntry(calendarEntry);
		
		List<CalendarEntry> calendarEntries = calendarService.getEntries(calendarEntry.getGroupId());
		
		CalendarEntry createdEntry = calendarEntries.stream()
				.filter(entry -> entry.getTitle().equals(calendarEntry.getTitle()))
				.findAny()
				.orElse(null);
		
		calendarService.deleteEntry(createdEntry.getEntryId());
		
		List<CalendarEntry> updatedCalendarEntries = calendarService.getEntries(calendarEntry.getGroupId());
		
		CalendarEntry deletedEntry = updatedCalendarEntries.stream()
				.filter(entry -> entry.getTitle().equals(createdEntry.getTitle()))
				.findAny()
				.orElse(null);
		
		assertNull(deletedEntry);
	}
	
	/**
	 * Test to ensure exception is thrown for stale update
	 * @throws Exception
	 */
	@Test
	public void staleUpdateTest() throws Exception {
		CalendarEntry calendarEntry = PropertyGeneratorTest.generateCalendarEntry();
		
		calendarService.createEntry(calendarEntry);
		
		List<CalendarEntry> calendarEntries = calendarService.getEntries(calendarEntry.getGroupId());
		
		CalendarEntry createdEntry = calendarEntries.stream()
				.filter(entry -> entry.getTitle().equals(calendarEntry.getTitle()))
				.findAny()
				.orElse(null);
		
		String updateDescription = "new description";
		String updateMessage = "Updated";
		
		createdEntry.setDescription(updateDescription);
		createdEntry.setUpdateComment(updateMessage);
		
		calendarService.updateEntry(createdEntry);
		
		List<CalendarEntry> updatedEntries = calendarService.getEntries(calendarEntry.getGroupId());
		
		CalendarEntry updatedEntry = updatedEntries.stream()
				.filter(entry -> entry.getTitle().equals(createdEntry.getTitle()))
				.findAny()
				.orElse(null);
		
		DateTime newTime = updatedEntry.getUpdateTimestamp();
		DateTime oldTime = newTime.minusSeconds(1);
		
		createdEntry.setDescription("any");
		createdEntry.setUpdateComment("comment");
		createdEntry.setUpdateTimestamp(oldTime);
		
		boolean success = false;
		
		try {
			calendarService.updateEntry(createdEntry);
		} catch(AppLayerException e) {
			success = true;
		}
		
		assertTrue(success);
	}
	
	/**
	 * Test to ensure exception thrown when a different 
	 * user tries to delete another user's entry
	 * @throws Exception
	 */
	@Test
	@Ignore
	public void unauthorizedDeletionTest() throws Exception {
		CalendarEntry calendarEntry = PropertyGeneratorTest.generateCalendarEntry();
		
		calendarService.createEntry(calendarEntry);
		
		List<CalendarEntry> calendarEntries = calendarService.getEntries(calendarEntry.getGroupId());
		
		CalendarEntry createdEntry = calendarEntries.stream()
				.filter(entry -> entry.getTitle().equals(calendarEntry.getTitle()))
				.findAny()
				.orElse(null);
		
		boolean success = false;
		
		calendarService.deleteEntry(createdEntry.getEntryId());
		
		assertTrue(success);
	}
}
