package co.agendaapp.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import co.agendaapp.model.User;
import co.agendaapp.model.Verified;
import co.agendaapp.dao.UserDetailsDao;
import co.agendaapp.util.PropertyGenerator;
import co.agendaapp.util.PropertyGeneratorTest;

/**
 * Unit test for sign up service
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 14 Apr 2016
 * @version 0.1
 */
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-application-context.xml")
public class SignUpServiceTest {

	@Autowired
	private SignUpService signUpService;

	@Autowired
	private UserDetailsDao userDetailsDao;

	@Before
	public void setUp() {}

	@Test
	public void signUpTest() {
		User user = PropertyGeneratorTest.generateEncryptedUser();

		String name = user.getName();
		String email = user.getEmail();
		String password = user.getPassword();
		String salt = user.getSalt();
		Verified verified = user.getIsVerified();

		signUpService.signUp(user);

		User user2 = userDetailsDao.getUserByEmail(email);

		String name2 = user2.getName();
		String email2 = user2.getEmail();
		String password2 = user2.getPassword();
		String salt2 = user2.getSalt();
		Verified verified2 = user2.getIsVerified();

		assertEquals(name, name2);
		assertEquals(email, email2);
		assertEquals(verified, verified2);
		assertEquals(password, password2);
		assertEquals(salt, salt2);
	}

	@Test
	public void verifyTest() throws Exception {
		User user = PropertyGenerator.generateUser();
		String email = user.getEmail();

		signUpService.signUp(user);

		User userAdded = userDetailsDao.getUserByEmail(email);
		int id = userAdded.getUserId();
		email = userAdded.getEmail();

		signUpService.verify(id);

		User userVerified = userDetailsDao.getUserByEmail(email);

		Verified verified = userAdded.getIsVerified();
		Verified verified2 = userVerified.getIsVerified();

		assertNotEquals(verified, verified2);
	}
}