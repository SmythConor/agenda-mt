package co.agendaapp.service;

import java.util.List;

import org.joda.time.DateTime;

import org.junit.Test;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import co.agendaapp.model.Message;
import co.agendaapp.util.PropertyGeneratorTest;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-application-context.xml")
public class MessageServiceTest {
	@Autowired
	private MessageService messageService;
	
	@Before
	public void setUp() {
		Message message = PropertyGeneratorTest.generateMessage();
		
		messageService.createMessage(message);
	}
	
	/**
	 * Test getting messages
	 * @throws Exception
	 */
	@Test
	public void getMessagesTest() throws Exception {
		List<Message> messages = messageService.getMessages(1);
		
		assertNotNull(messages);
	}
	
	/**
	 * Test creating a message
	 * @throws Exception
	 */
	@Test
	public void createMessageTest() throws Exception {
		Message message = PropertyGeneratorTest.generateMessage();
		
		messageService.createMessage(message);
		
		List<Message> messages = messageService.getMessages(message.getGroupId());
		
		Message createdMessage = messages
				.stream()
				.filter(entry -> entry.getMessageId().equals(message.getMessageId()))
				.findAny()
				.orElse(null);
		
		assertNotNull(createdMessage);
		
		String beforeMessageBody = message.getMessageBody();
		Integer beforeMessageSenderId = message.getSenderId();
		Integer beforeGroupId = message.getGroupId();
		DateTime beforeSentTimestamp = message.getSentTimestamp();
		
		String afterMessageBody = message.getMessageBody();
		Integer afterMessageSenderId = message.getSenderId();
		Integer afterMessageGroupId = message.getGroupId();
		DateTime afterSentTimestamp = message.getSentTimestamp();
		DateTime afterDeliveredTimestamp = message.getDeliveredTimestamp();
		
		assertNull(afterDeliveredTimestamp);
		
		assertEquals(beforeMessageBody, afterMessageBody);
		assertEquals(beforeGroupId, afterMessageGroupId);
		assertEquals(beforeMessageSenderId, afterMessageSenderId);
		assertEquals(beforeSentTimestamp, afterSentTimestamp);
	}
	
	/**
	 * Test updating a message
	 * @throws Exception
	 */
	@Test
	public void updateMessageTest() throws Exception {
		Message message = PropertyGeneratorTest.generateMessage();
		
		messageService.createMessage(message);
		
		List<Message> messages = messageService.getMessages(message.getGroupId());
		
		Message createdMessage = messages
				.stream()
				.filter(entry -> entry.getMessageId().equals(message.getMessageId()))
				.findAny()
				.orElse(null);
		
		DateTime now = DateTime.now();
		
		createdMessage.setDeliveredTimestamp(now);
		
		messageService.updateMessage(createdMessage);
		
		List<Message> updatedMessages = messageService.getMessages(createdMessage.getGroupId());
		
		Message updatedMessage = updatedMessages
				.stream()
				.filter(entry -> entry.getMessageId().equals(createdMessage.getMessageId()))
				.findAny()
				.orElse(null);
		
		assertNotNull(updatedMessage.getDeliveredTimestamp());
	}
}