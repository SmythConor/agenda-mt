package co.agendaapp.service;

import co.agendaapp.exception.AppLayerException;
import co.agendaapp.model.User;

/**
 * Log in service interface
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 2016-02-17
 * @version 0.1
 */
public interface LogInService {
	/**
	 * Log in interface for log in service
	 * @param user User to log in
	 * @return user logged in
	 * @throws AppLayerException
	 */
	public User logIn(User user) throws AppLayerException;
}