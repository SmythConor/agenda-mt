package co.agendaapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import co.agendaapp.dao.UserDetailsDao;
import co.agendaapp.encrypt.EncryptionUtils;
import co.agendaapp.encrypt.Encryptor;
import co.agendaapp.exception.AppLayerException;
import co.agendaapp.model.User;
/**
 * Log in service implementation 
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 2016-02-17
 * @version 0.1
 */
@Service
public class LogInServiceImpl implements LogInService {
	
	@Autowired
	private UserDetailsDao userDetailsDao;

	/**
	 * Log In service implementation
	 * @param user User to log in
	 * @throws AppLayerException
	 */
	public User logIn(User user) throws AppLayerException {
		User dbEntry = userDetailsDao.getUserByEmail(user.getEmail());

		if(dbEntry == null) {
			throw new AppLayerException("User does not exist", HttpStatus.NOT_FOUND);
		}
		// Verified vEntry = entry.getIsVerified();

		/*
		 * if(vEntry.equals(Verified.NOT)) { //leaving out this for now throw
		 * new AppLayerException("Not verified", HttpStatus.UNAUTHORIZED; }
		 */

		String inputPassword = user.getPassword();
		String storedSalt = dbEntry.getSalt();

		byte[] bytePassword = EncryptionUtils.stringToBytes(inputPassword);
		byte[] salt = EncryptionUtils.hexToBytes(storedSalt);

		String storedPassword = dbEntry.getPassword();
		String hashedPassword = Encryptor.hashPassword(bytePassword, salt);

		if(!hashedPassword.equals(storedPassword)) {
			throw new AppLayerException("Incorrect Password", HttpStatus.UNPROCESSABLE_ENTITY);
		}
		
		User returnUser = new User();
		
		returnUser.setUserId(dbEntry.getUserId());
		returnUser.setName(dbEntry.getName());
		returnUser.setEmail(dbEntry.getEmail());
		returnUser.setIsVerified(dbEntry.getIsVerified());
		
		return returnUser;
	}
}
