package co.agendaapp.service;

import java.util.List;

import co.agendaapp.model.ToDoListEntry;
import co.agendaapp.exception.AppLayerException;

/**
 * Service interface for To-Do list 
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 14 Apr 2016
 * @version 0.1
 */
public interface ToDoListService {
	
	/**
	 * Service functions to get entries in the to do list
	 * @param groupId the id of the group to get
	 * @return the list of to do list entries
	 */
	public List<ToDoListEntry> getEntries(Integer groupId);
	
	/**
	 * Service function to create a to do list entry
	 * @param toDoListEntry the entry to create
	 * @return the created to-do list entry
	 */
	public ToDoListEntry createEntry(ToDoListEntry toDoListEntry);
	
	/**
	 * Service function to update a to do list entry
	 * @param toDoListEntry the entry to update
	 * @return the updated entry
	 */
	public ToDoListEntry updateEntry(ToDoListEntry toDoListEntry) throws AppLayerException;
	
	/**
	 * Service function to delete a to do list entry
	 * @param entryId the id of the entry to delete
	 */
	public void deleteEntry(Integer entryId) throws AppLayerException;
}
