package co.agendaapp.service;

import java.util.List;

import co.agendaapp.model.Message;

/**
 * Message Service interface
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 2016-05-26
 */
public interface MessageService {
	/**
	 * Service function to get messages for a group
	 * @param groupId the id of the group to get
	 * @return the message for the group as a list
	 */
	public List<Message> getMessages(Integer groupId);
	
	/**
	 * Service function to create a message
	 * @param message the message to create
	 */
	public void createMessage(Message message);
	
	/**
	 * Service function to update a message
	 * used for inserting delivered timestamp
	 * @param message the message to update
	 */
	public void updateMessage(Message message);
}
