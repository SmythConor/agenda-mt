package co.agendaapp.service;

import java.util.List;

import co.agendaapp.exception.AppLayerException;
import co.agendaapp.model.Group;
import co.agendaapp.model.User;

/**
 * Group Service interface
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 2016-02-25
 * @version 0.1
 */
public interface GroupService {
	
	/**
	 * Service interface to get the groups that a user is associated with
	 * @param userId the id of the user to get the groups
	 * @return list of groups associated the user 
	 */
	public List<Group> getGroups(Integer userId);
	
	/**
	 * Service intergace to get a group
	 * @param groupId the id of the group to get
	 * @return the group associated with group id passed
	 * @throws AppLayerException
	 */
	public Group getGroup(Integer groupId) throws AppLayerException;
	
	/**
	 * Service interface to create a group 
	 * @param group the group to create
	 * @return the created group
	 */
	public Group createGroup(Group group);
	
	/**
	 * Service interface to add a member to a group
	 * @param user the user to add to the group
	 * @return registration id as string
	 * @throws AppLayerException
	 */
	public String addMember(Integer groupId, User user) throws AppLayerException;
	
	/**
	 * Service interface to remove a member from a group
	 * @param groupId the id of the group
	 * @param email the user's email to remove
	 * @throws AppLayerException
	 */
	public void removeMember(Integer groupId, String email) throws AppLayerException;
	
	/**
	 * Service Interface to update the group name
	 * @param groupId the group id
	 * @param name the name to change
	 */
	public void updateName(Integer groupId, String name);
}
