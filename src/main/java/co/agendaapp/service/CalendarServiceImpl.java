package co.agendaapp.service;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import co.agendaapp.dao.CalendarDao;
import co.agendaapp.model.CalendarEntry;
import co.agendaapp.exception.AppLayerException;

@Service
public class CalendarServiceImpl implements CalendarService {
	
	@Autowired
	private CalendarDao calendarDao;
	
	/**
	 * {@inheritDoc}
	 */
	public List<CalendarEntry> getEntries(Integer groupId) {
		List<CalendarEntry> calendarEntries = calendarDao.getEntries(groupId);
		
		return calendarEntries;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public CalendarEntry createEntry(CalendarEntry calendarEntry) {
		calendarDao.createEntry(calendarEntry);
		
		return calendarEntry;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public CalendarEntry updateEntry(CalendarEntry calendarEntry) throws AppLayerException {
		CalendarEntry entryToUpdate = calendarDao.getEntriesById(calendarEntry.getEntryId());
		
		if(entryToUpdate.getUpdateTimestamp().isAfter(calendarEntry.getUpdateTimestamp())) {
			throw new AppLayerException("Stale update", HttpStatus.UNPROCESSABLE_ENTITY);
		}
		
		calendarDao.updateEntry(calendarEntry);
		
		return calendarEntry;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void deleteEntry(Integer entryId) {
		calendarDao.deleteEntry(entryId);
	}
}
