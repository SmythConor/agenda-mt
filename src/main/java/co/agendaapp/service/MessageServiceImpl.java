package co.agendaapp.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import co.agendaapp.model.Message;
import co.agendaapp.dao.MessageDao;

/**
 * Message Service implementation
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 2016-05-26
 */
@Service
public class MessageServiceImpl implements MessageService {
	
	@Autowired
	private MessageDao messageDao;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Message> getMessages(Integer groupId) {
		List<Message> messages = messageDao.getMessages(groupId);
		
		return messages;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void createMessage(Message message) {
		messageDao.createMessage(message);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateMessage(Message message) {
		messageDao.updateMessage(message);
	}

}
