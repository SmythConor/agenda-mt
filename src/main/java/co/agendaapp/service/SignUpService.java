package co.agendaapp.service;

import org.springframework.dao.DuplicateKeyException;

import co.agendaapp.model.User;

/**
 * Sign up service interface
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-02-05
 */
public interface SignUpService {
	
	/**
	 * Sign up service for initial registration
	 * @param user The user to register
	 * @throws DuplicateKeyException
	 */
	public void signUp(User user) throws DuplicateKeyException;

	/**
	 * Sign up service for user registration
	 * @param userId The user id for the user to verify
	 */
	public void verify(Integer userId);
}