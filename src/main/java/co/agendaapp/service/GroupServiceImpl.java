package co.agendaapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import co.agendaapp.dao.GroupDao;
import co.agendaapp.dao.UserDetailsDao;
import co.agendaapp.exception.AppLayerException;
import co.agendaapp.model.Group;
import co.agendaapp.model.User;

/**
 * Group service implementation
 * 
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 2016-02-25
 * @version 0.1
 */
@Service
public class GroupServiceImpl implements GroupService {

	@Autowired
	private GroupDao groupDao;

	@Autowired
	private UserDetailsDao userDetailsDao;

	/**
	 * {@inheritDoc}
	 */
	public List<Group> getGroups(Integer userId) {
		return groupDao.getGroups(userId);
	}

	/**
	 * {@inheritDoc}
	 */
	public Group getGroup(Integer groupId) throws AppLayerException {
		throw new AppLayerException("Group doesn't exist",
		        HttpStatus.NOT_FOUND);
	}

	/**
	 * {@inheritDoc}
	 */
	public Group createGroup(Group group) {
		Group grp = groupDao.createGroup(group);

		return grp;
	}

	/**
	 * {@inheritDoc}
	 */
	public String addMember(Integer groupId, User user)
	        throws AppLayerException {
		User existingUser = userDetailsDao.getUserByEmail(user.getEmail());

		if(existingUser == null) {
			throw new AppLayerException("User does not exist",
			        HttpStatus.NOT_FOUND);
		}

		Integer userId = existingUser.getUserId();

		groupDao.addMember(groupId, userId);

		return existingUser.getRegistrationId();
	}

	/**
	 * {@inheritDoc}
	 */
	public void removeMember(Integer groupId, String email)
	        throws AppLayerException {
		User user = userDetailsDao.getUserByEmail(email);

		if(user == null) {
			throw new AppLayerException("User not exist", HttpStatus.NOT_FOUND);
		}

		List<Group> groups = groupDao.getGroups(user.getUserId());

		Group group = groups.stream()
		        .filter(entry -> entry.getGroupId() == groupId).findAny()
		        .orElse(null);
		System.out.println(group);
		
		if(group == null) {
			throw new AppLayerException("User not in group",
			        HttpStatus.NOT_FOUND);
		}
		
		groupDao.removeMember(groupId, user.getUserId());
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void updateName(Integer groupId, String name) {
		groupDao.updateName(groupId, name);
	}
}
