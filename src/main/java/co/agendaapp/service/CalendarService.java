package co.agendaapp.service;

import co.agendaapp.model.CalendarEntry;

import java.util.List;

import co.agendaapp.exception.AppLayerException;

public interface CalendarService {
	
	/**
	 * Service function to get the calendar entries for a group
	 * @param groupId the id of the group
	 * @return calendar entries for the group as a list
	 */
	public List<CalendarEntry> getEntries(Integer groupId);
	
	/**
	 * Service function to create a calendar entry for a group
	 * @param calendarEntry the entry to create
	 * @return the created entry
	 */
	public CalendarEntry createEntry(CalendarEntry calendarEntry);
	
	/**
	 * Service function to update a calendar entry for a group
	 * @param calendarEntry the entry to update with
	 * @return the updated entry
	 * @throws AppLayerException for stale update
	 */
	public CalendarEntry updateEntry(CalendarEntry calendarEntry) throws AppLayerException;
	
	/**
	 * Service function to delete a calendar entry for a group
	 * @param entryId the entry to delete
	 * @throws AppLayerException for unauthorized deletion
	 */
	public void deleteEntry(Integer entryId);
}
