package co.agendaapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import co.agendaapp.dao.SignUpDao;
import co.agendaapp.model.User;

/**
 * Sign up service implementation
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-02-05
 */
@Service("signUpService")
public class SignUpServiceImpl implements SignUpService {
	
	@Autowired
	private SignUpDao signUpDao;

	/**
	 * Sign up service implementation for user registration
	 * @param user The user to register
	 * @throws DuplicateKeyException
	 */
	public void signUp(User user) throws DuplicateKeyException {
		signUpDao.signUp(user);
	}

	/**
	 * Sign up service implementation for user verification
	 * @param userId The id of the user for registration
	 */
	public void verify(Integer userId) {
		signUpDao.verify(userId);
	}
}