package co.agendaapp.service;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import co.agendaapp.dao.ToDoListDao;
import co.agendaapp.exception.AppLayerException;
import co.agendaapp.model.ToDoListEntry;

/**
 * Service implementation for to do list entries
 * 
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 14 Apr 2016
 * @version 0.1
 */
@Service
public class ToDoListServiceImpl implements ToDoListService {

	@Autowired
	private ToDoListDao toDoListDao;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ToDoListEntry> getEntries(Integer groupId) {
		List<ToDoListEntry> toDoListEntries = toDoListDao.getEntries(groupId);

		return toDoListEntries;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ToDoListEntry createEntry(ToDoListEntry toDoListEntry) {
		toDoListDao.createEntry(toDoListEntry);

		return toDoListEntry;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ToDoListEntry updateEntry(ToDoListEntry toDoListEntry)
	        throws AppLayerException {
		ToDoListEntry dbToDoListEntry = toDoListDao
		        .getEntriesById(toDoListEntry.getEntryId());
		
		DateTime dbUpdateTimestamp = dbToDoListEntry.getUpdateTimestamp();
		DateTime updateTimestamp = toDoListEntry.getUpdateTimestamp();
		
		if(dbUpdateTimestamp.isAfter(updateTimestamp)) {
			throw new AppLayerException("Stale update", HttpStatus.UNPROCESSABLE_ENTITY);
		}
		
		toDoListDao.updateEntry(toDoListEntry);

		return toDoListEntry;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void deleteEntry(Integer entryId)
	        throws AppLayerException {
		ToDoListEntry toDoListEntry = toDoListDao.getEntriesById(entryId);

		toDoListDao.deleteEntry(entryId);
	}
}
