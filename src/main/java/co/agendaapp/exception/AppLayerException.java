package co.agendaapp.exception;

import java.io.Serializable;

import org.springframework.http.HttpStatus;

/**
 * Generic exception handler
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 21 Mar 2016
 * @version 0.1
 */
public class AppLayerException extends Exception implements Serializable {
	private static final long serialVersionUID = 6987393457309116469L;
	
	private String message;
	private HttpStatus status;
	
	public AppLayerException() {
		super("Unknown error");
	}
	
	/**
	 * @param message
	 * @param status
	 */
	public AppLayerException(String message, HttpStatus status) {
		this.message = message;
		this.status = status;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * @return the status
	 */
	public HttpStatus getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AppLayerException [");
		if(message != null) {
			builder.append("message=");
			builder.append(message);
			builder.append(", ");
		}
		if(status != null) {
			builder.append("status=");
			builder.append(status);
		}
		builder.append("]");
		return builder.toString();
	}
}