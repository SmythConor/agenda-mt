package co.agendaapp.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Controller to handle exception and return appropriate message
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 21 Mar 2016
 * @version 0.1
 */
@ControllerAdvice
public class AppLayerExceptionHandler extends ResponseEntityExceptionHandler {
	
	final Logger logger = LoggerFactory.getLogger(AppLayerExceptionHandler.class); 
	
	/**
	 * Handle custom exceptions
	 * @param ex the exception thrown
	 * @return ResponseEntity with error message and status
	 */
	@ExceptionHandler(AppLayerException.class)
    public ResponseEntity<Errors> handleAppLayerException(AppLayerException ex) {
		Errors err = new Errors();
		HttpStatus status = ex.getStatus();
		
		err.setMessage(ex.getMessage());
		
		logger.error(err.toString());
        
        return new ResponseEntity<Errors>(err, status);
    }
	
	/**
	 * Duplicate key handler
	 * @param ex the exception thrown
	 * @return ResponseEntity with the error message and status
	 */
	@ExceptionHandler(DuplicateKeyException.class)
	public ResponseEntity<Errors> handleDuplicateKeyException(DuplicateKeyException ex) {
		Errors err = new Errors();
		HttpStatus status = HttpStatus.UNPROCESSABLE_ENTITY;
		
		String errorMessage = "Error message: User already exists";
		
		err.setMessage(errorMessage);
		
		return new ResponseEntity<Errors>(err, status);
	}
}