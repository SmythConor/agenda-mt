package co.agendaapp.util;

import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.MappedTypes;
import org.apache.ibatis.type.TypeHandler;

import org.joda.time.DateTime;

import java.sql.Timestamp;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;

@SuppressWarnings("rawtypes")
@MappedTypes(DateTime.class)
public class DateTimeTypeHandler implements TypeHandler {

	@Override
	public Object getResult(ResultSet rs, String columnName)
	        throws SQLException {
		Timestamp rowValue = rs.getTimestamp(columnName);

		return rowValue != null ? new DateTime(rowValue.getTime()) : null;
	}

	@Override
	public Object getResult(ResultSet rs, int columnIndex) throws SQLException {
		return null;
	}

	@Override
	public Object getResult(CallableStatement cs, int columnIndex)
	        throws SQLException {
		return null;
	}

	@Override
	public void setParameter(PreparedStatement ps, int columnIndex,
	        Object parameter, JdbcType jdbcType) throws SQLException {
		DateTime dateTime = (DateTime) parameter;
		if(dateTime != null) {
			long timeInMillis = dateTime.getMillis();

			Timestamp date = new Timestamp(timeInMillis);

			ps.setTimestamp(columnIndex, date);
		} else {
			ps.setTimestamp(columnIndex, null);
		}
	}
}