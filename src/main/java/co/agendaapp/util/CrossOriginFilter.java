package co.agendaapp.util;

import org.springframework.context.annotation.Bean;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;

import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
public class CrossOriginFilter extends WebMvcConfigurerAdapter {

	private final String URL = "http://127.0.0.1:8080";

	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurerAdapter() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/signUp/register").allowedOrigins(URL);
				registry.addMapping("/logIn").allowedOrigins(URL);
				registry.addMapping("/group/getGroups/*").allowedOrigins(URL);
				registry.addMapping("/group/createGroup").allowedOrigins(URL);
				registry.addMapping("/calendar/createEntry").allowedOrigins(URL);
			}
		};
	}
}
