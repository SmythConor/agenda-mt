package co.agendaapp.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.common.base.Predicate;
import static com.google.common.base.Predicates.or;

import static springfox.documentation.builders.PathSelectors.regex;

import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	@Bean
	public Docket generateApi() {
		List<ResponseMessage> responseMessages = getResponseMessages();
		
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
					.paths(getPaths())
		        .build()
				.pathMapping("/")
				.apiInfo(getApiInfo())
				.produces(Collections.singleton("application/json"))
				.consumes(Collections.singleton("application/json"))
				.useDefaultResponseMessages(false)
				.globalResponseMessage(RequestMethod.GET, responseMessages)
				.globalResponseMessage(RequestMethod.POST, responseMessages)
				.globalResponseMessage(RequestMethod.PUT, responseMessages)
				.globalResponseMessage(RequestMethod.DELETE, responseMessages);
	}
	
	private ApiInfo getApiInfo() {
		return null;
	}
	
	private Predicate<String> getPaths() {
		return or(
				regex("/signUp/*"), 
				regex("logIn/*")
				);
	}
	
	private List<ResponseMessage> getResponseMessages() {
		ResponseMessage responseMessage200 = new ResponseMessageBuilder().code(200).message("Success").build();
		ResponseMessage responseMessage201 = new ResponseMessageBuilder().code(201).message("Created").build();
		ResponseMessage responseMessage404 = new ResponseMessageBuilder().code(404).message("Not Found").build();
		ResponseMessage responseMessage500 = new ResponseMessageBuilder().code(500).message("Server Error").build();
		
		List<ResponseMessage> ls = Arrays.asList(responseMessage200, responseMessage201, 
				responseMessage404, responseMessage500);
		
		return ls;
	}
}
