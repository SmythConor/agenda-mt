package co.agendaapp.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import co.agendaapp.model.Group;
import co.agendaapp.model.User;
import co.agendaapp.model.Verified;

public class PropertyGenerator {
	public static String generateString() {
		String str = "test-";
		int num = new Random().nextInt(100000) + 1;
		
		return str + num;
	}
	
	public static User generateUser() {
		String name = PropertyGenerator.generateString();
		String email = PropertyGenerator.generateString() + "@gmail.com";
		String password = PropertyGenerator.generateString();
		String salt = PropertyGenerator.generateString();
		
		return new User(0, name, email, password, salt, Verified.NOT, "");
	}
	
	public static Group generateGroup() {
		User user = generateUser();
		String name = generateString();
		String key = generateString();
		String keyName = generateString();
		
		user.setUserId(1);
		
		Group g = new Group();
		
		g.setGroupId(0);
		g.setGroupAdminId(user.getUserId());
		g.setName(name);
		g.setNotificationKey(key);
		g.setNotificationKeyName(keyName);
		
		
		//return new Group(0, user.getUserId(), name);
		return g;
	}
	
	public static List<Group> generateGroups() {
		List<Group> groups = new ArrayList<Group>();
		
		for(int i = 0; i < 10; i++) {
			Group g = generateGroup();
			
			groups.add(g);
		}
		
		return groups;
	}
}
