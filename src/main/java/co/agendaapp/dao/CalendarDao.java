package co.agendaapp.dao;

import java.util.List;

import co.agendaapp.model.CalendarEntry;

public interface CalendarDao {
	
	/**
	 * Dao interface to get the calendar entries for a group
	 * @param groupId the id of the group
	 * @return list of calendar entries
	 */
	public List<CalendarEntry> getEntries(Integer groupId);
	
	/**
	 * Dao interface to create a calender entry 
	 * @param calendarEntry the entry to create
	 * @return the created calendar entry
	 */
	public CalendarEntry createEntry(CalendarEntry calendarEntry);
	
	/**
	 * Dao interface to update a calendar entry
	 * @param calendarEntry the entry to update
	 * @return the updated calendar entry
	 */
	public CalendarEntry updateEntry(CalendarEntry calendarEntry);
	
	/**
	 * Dao interface to delete a calendar entry
	 * @param entryId the id of the entry to delete
	 * @param userId the id of the user making the request
	 * @return the deleted calender entry
	 */
	public CalendarEntry deleteEntry(Integer entryId);
	
	/**
	 * Dao interface to get entries by id
	 * @param entryId the entry to get
	 * @return the calendar entry with id supplied
	 */
	public CalendarEntry getEntriesById(Integer entryId);
}