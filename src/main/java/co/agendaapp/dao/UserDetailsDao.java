package co.agendaapp.dao;

import co.agendaapp.model.User;

public interface UserDetailsDao {
	
	/**
	 * Semi-util DAO interface for getting user details
	 * @param email
	 * @return User associated with the email
	 */
	public User getUserByEmail(String email);
}