package co.agendaapp.dao;

import java.util.List;

import co.agendaapp.model.Group;

import org.apache.ibatis.annotations.Param;

public interface GroupDao {
	
	/**
	 * Dao function to retrieve the groups for a user ID
	 * @param userId the id of the user of the groups
	 * @return list of groups the user is a member of
	 */
	public List<Group> getGroups(Integer userId);
	
	/**
	 * May not need this, leaving for now
	 * Dao function to retrieve the group selected
	 * @param groupId the id of the group to get
	 * @return group with the selected group ID
	 */
	public Group getGroup(Integer groupId);
	
	/**
	 * Dao function to create a group
	 * @param group the group to create
	 * @return the group created
	 */
	public Group createGroup(Group group);
	
	/**
	 * Add a user to a group
	 * @param user the user to add
	 */
	public void addMember(
			@Param("groupId") Integer groupId,
			@Param("userId") Integer user);

	/**
	 * Dao function to remove a member form a group
	 * @param groupId the id of the group to remove from
	 * @param userId the id of the user to remove
	 */
	public void removeMember(
			@Param("groupId") Integer groupId,
			@Param("userId") Integer userId);
	
	/**
	 * Dao function to update the group name
	 * @param groupId the id of the group
	 * @param name the name to change it to
	 */
	public void updateName(
			@Param("groupId") Integer groupId,
			@Param("name") String name);
}