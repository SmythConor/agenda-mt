package co.agendaapp.dao;

import java.util.List;

import co.agendaapp.model.ToDoListEntry;

/**
 * Dao interface for to do list
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 14 Apr 2016
 * @version 0.1
 */
public interface ToDoListDao {
	
	/**
	 * Dao interface for getting to do list entries for a group
	 * @param groupId the id of the group
	 * @return the list of to do list entries
	 */
	public List<ToDoListEntry> getEntries(Integer groupId);
	
	/**
	 * Dao interface for creating a to do list entry
	 * @param toDoListEntry the entry to create
	 * @return the created to do list entry
	 */
	public ToDoListEntry createEntry(ToDoListEntry toDoListEntry);

	/**
	 * Dao interface for updating a to do list entry
	 * @param toDoListEntry the entry to update
	 * @return the updated entry
	 */
	public ToDoListEntry updateEntry(ToDoListEntry toDoListEntry);
	
	/**
	 * Dao interface for deleting a to do list entry
	 * @param entryId the id of the entry to delete
	 * @return the deleted entry
	 */
	public ToDoListEntry deleteEntry(Integer entryId);
	
	/**
	 * Dao interface for getting a to do list entry by id
	 * @param entryId the id to get
	 * @return The to do list entry
	 */
	public ToDoListEntry getEntriesById(Integer entryId);
}