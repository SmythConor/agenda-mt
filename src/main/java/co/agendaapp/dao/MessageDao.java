package co.agendaapp.dao;

import java.util.List;

import co.agendaapp.model.Message;

/**
 * Message Dao interface
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 2016-05-26
 */
public interface MessageDao {
	
	/**
	 * DAO interface to get the messages for a group
	 * @param groupId
	 * @return
	 */
	public List<Message> getMessages(Integer groupId);
	
	/**
	 * DAO interface to create a message
	 * @param message the message to create
	 */
	public void createMessage(Message message);
	
	/**
	 * DAO interface to update a message
	 * @param message the message to update
	 */
	public void updateMessage(Message message);
}