package co.agendaapp.dao;

import co.agendaapp.model.User;

/**
 * Sign up DAO class for database access
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-02-05
 */
public interface SignUpDao {
	/**
	 * Sign up DAO interface for user registration
	 * @param user The user to be registered
	 */
	public void signUp(User user);
	
	/**
	 * Sign up DAO interface for user verification
	 * @param userId The user id of the user to verify
	 */
	public void verify(Integer userId);
}