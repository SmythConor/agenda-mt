package co.agendaapp.controller;

import org.springframework.beans.factory.annotation.Autowired;

import static org.springframework.http.HttpStatus.OK;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import co.agendaapp.exception.AppLayerException;
import co.agendaapp.model.User;
import co.agendaapp.service.LogInService;

/**
 * Controller for user log in
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 2016-02-25
 * @version 0.2
 */
@Controller
public class LogInController {

	@Autowired
	private LogInService logInService;

	/**
	 * Controller for user log in
	 * @param user the user to sign in
	 * @return The user signed in
	 * @throws AppLayerException
	 */
	@RequestMapping(value = "/logIn")	
	public ResponseEntity<User> logIn(@RequestBody User user) throws AppLayerException {
		User returnUser = logInService.logIn(user);

		return new ResponseEntity<User>(returnUser, OK);
	}
}