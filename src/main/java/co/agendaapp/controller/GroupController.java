package co.agendaapp.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.agendaapp.exception.AppLayerException;
import co.agendaapp.model.User;
import co.agendaapp.model.Group;
import co.agendaapp.service.GroupService;

/**
 * Controller for groups
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 2016-02-25
 * @version 0.2
 */
@Controller
@RequestMapping(value = "/group")
public class GroupController {
	
	@Autowired	
	private GroupService groupService;
	
	/**
	 * Controller to get the groups for a user
	 * @param userId the id of the user associated to the groups
	 * @return the list of groups for the user
	 */
	@RequestMapping(value = "/getGroups/{userId}", method = GET)
	public ResponseEntity<List<Group>> getGroups(@PathVariable Integer userId) {
		List<Group> groups = groupService.getGroups(userId);
		
		return new ResponseEntity<List<Group>>(groups, HttpStatus.OK);
	}
	
	/**
	 * Controller to get the group for a group id 
	 * @param groupId the group ID to get
	 * @return the groups associated with the group ID
	 * @throws AppLayerException
	 */
	@RequestMapping(value = "/getGroup/{groupId}", method = GET) 
	public ResponseEntity<Group> getGroup(@PathVariable Integer groupId) throws AppLayerException {
		Group returnGroup = new Group();
		
		returnGroup = groupService.getGroup(groupId);
		
		return new ResponseEntity<Group>(returnGroup, HttpStatus.OK);
	}
	
	/**
	 * Controller to create a group
	 * @param group the group to create
	 * @return the created group
	 */
	@RequestMapping(value = "/createGroup", method = POST)
	public ResponseEntity<Group> createGroup(@RequestBody Group group) {
		groupService.createGroup(group);
		System.out.println(group);
		
		return new ResponseEntity<Group>(group, HttpStatus.CREATED);
	}
	
	/**
	 * Controller to add a member to group
	 * @param email the email address of the user
	 * @return a success message
	 * @throws AppLayerException
	 */
	@RequestMapping(value = "/addMember/{groupId}", method = PUT)
	public ResponseEntity<String> addMember(@PathVariable Integer groupId, 
			@RequestBody User user) throws AppLayerException {
		String registrationId = groupService.addMember(groupId, user);
		
		String success = "";
		
		try {
			success = new ObjectMapper().writeValueAsString(registrationId);
		} catch(JsonProcessingException e) {
			throw new AppLayerException("Something messed up", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<String>(success, HttpStatus.ACCEPTED);
	}
	
	@RequestMapping(value = "/removeMember/{groupId}", method = DELETE)
	public ResponseEntity<String> removeMember(@PathVariable Integer groupId, 
			@RequestBody User user) throws AppLayerException {
		
		String email = user.getEmail();
		
		groupService.removeMember(groupId, email);
		
		String success = "";
		
		try {
			success = new ObjectMapper().writeValueAsString("success");
		} catch(JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return new ResponseEntity<String>(success, HttpStatus.OK);
	}
	
	/**
	 * Update name controller to handle updating a group name
	 * @param groupId the id of the group to update
	 * @param name the name to update with
	 * @return success
	 * @throws AppLayerException
	 */
	@RequestMapping(value = "/updateName/{groupId}", method = PUT)
	public ResponseEntity<String> updateName(@PathVariable Integer groupId, 
			@RequestBody String name) throws AppLayerException {
		
		groupService.updateName(groupId, name);
		
		String success = "";
		
		try {
			success = new ObjectMapper().writeValueAsString("success");
		} catch(JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return new ResponseEntity<String>(success, HttpStatus.OK);
	}
}
