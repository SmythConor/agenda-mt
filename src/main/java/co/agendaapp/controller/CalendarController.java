package co.agendaapp.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

import co.agendaapp.model.CalendarEntry;
import co.agendaapp.service.CalendarService;
import co.agendaapp.exception.AppLayerException;

@Controller
@RequestMapping(value = "/calendar")
public class CalendarController {
	final Logger logger = LoggerFactory.getLogger(CalendarController.class);
	
	@Autowired
	private CalendarService calendarService;

	/**
	 * Controller function to get entries for a group
	 * @param groupId id of the group
	 * @return the groups entries
	 */
	@RequestMapping(value = "/getEntries/{groupId}", method = GET)
	public ResponseEntity<List<CalendarEntry>> getCalendarEntries(@PathVariable Integer groupId) {
		
		List<CalendarEntry> calendarEntries = calendarService.getEntries(groupId);

		return new ResponseEntity<>(calendarEntries, HttpStatus.OK);
	}

	/**
	 * Controller function to create an entry for a group
	 * @param calendarEntry the calendar entry to create
	 * @return the created calendar entry
	 */
	@RequestMapping(value = "/createEntry", method = POST)
	public ResponseEntity<CalendarEntry> createCalendarEntry(@RequestBody CalendarEntry calendarEntry) {
		calendarService.createEntry(calendarEntry);

		return new ResponseEntity<>(calendarEntry, HttpStatus.CREATED);
	}

	/**
	 * Controller function to update a calendar entry for a group
	 * @param calendarEntry the calendar entry to update with
	 * @return the updated calendar entry
	 * @throws AppLayerException for stale update
	 */
	@RequestMapping(value = "/updateEntry", method = PUT)
	public ResponseEntity<CalendarEntry> updateCalendarEntry(@RequestBody CalendarEntry calendarEntry) 
			throws AppLayerException {
		calendarService.updateEntry(calendarEntry);

		return new ResponseEntity<>(calendarEntry, HttpStatus.OK);
	}

	/**
	 * Controller function to delete a calendar entry for a group
	 * @param entryId the id of the entry to delete
	 * @return success
	 * @throws AppLayerException for unauthorized user
	 */
	@RequestMapping(value = "/deleteEntry/{entryId}", method = DELETE)
	public ResponseEntity<String> deleteCalendarEntry(@PathVariable Integer entryId) {
		calendarService.deleteEntry(entryId);
		
		String success = "success";
		
		try {
			success = new ObjectMapper().writeValueAsString("success");
		} catch(JsonProcessingException e) {
			logger.error(e.getMessage());
		}

		return new ResponseEntity<>(success, HttpStatus.OK);
	}
}
