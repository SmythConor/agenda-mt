package co.agendaapp.controller;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.dao.DuplicateKeyException;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

import co.agendaapp.model.User;
import co.agendaapp.service.SignUpService;
import io.swagger.annotations.ApiOperation;

/**
 * Sign up controller class for user registration
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.2
 * @since 2016-02-05
 */
@Controller
@RequestMapping("/signUp")
public class SignUpController {

	@Autowired
	private SignUpService signUpService;

	/**
	 * Sign up controller for initial registration of a user
	 * @param user The user to be registered
	 * @return the user signed up
	 */
	@ApiOperation(value = "Register a user", nickname = "")
	@RequestMapping(value = "/register", method = POST)
	public ResponseEntity<User> signUp(@RequestBody User user) throws DuplicateKeyException {
		String name = user.getName();
		String email = user.getEmail();
		
		User returnUser = new User();
		returnUser.setName(name);
		returnUser.setEmail(email);
		
		signUpService.signUp(user);

		return new ResponseEntity<User>(returnUser, CREATED);
	}

	/**
	 * Sign up controller for verification of a user
	 * This does not work at the moment
	 * @param userId the user id for the user to be verified
	 * @return
	 */
	@RequestMapping(value = "/verify/{userId}", method = POST)
	public ResponseEntity<Integer> verify(@PathVariable Integer userId) {
		signUpService.verify(userId);

		return new ResponseEntity<Integer>(userId, OK);
	}
}