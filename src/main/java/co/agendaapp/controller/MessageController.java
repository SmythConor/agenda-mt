package co.agendaapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import co.agendaapp.model.Message;
import co.agendaapp.service.MessageService;

@Controller
@RequestMapping(value = "/message")
public class MessageController {
	
	@Autowired
	private MessageService messageService;
	
	/**
	 * Controller function to get the messages for a group
	 * @param groupId the id of the group
	 * @return list of messages for specified group
	 */
	@RequestMapping(value = "/getMessages/{groupId}", method = RequestMethod.GET)
	public ResponseEntity<List<Message>> getMessages(@PathVariable Integer groupId) {
		List<Message> messages = messageService.getMessages(groupId);
		
		return new ResponseEntity<>(messages, HttpStatus.OK);
	}
	
	/**
	 * Controller function to create a message
	 * @param message the message to create
	 * @return the created message
	 */
	@RequestMapping(value = "/createMessage", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	public ResponseEntity<Message> createMessgae(@RequestBody Message message) {
		messageService.createMessage(message);
		
		return new ResponseEntity<>(message, HttpStatus.CREATED);
	}
	
	/**
	 * Controller function to update a message
	 * @param message the message to update
	 * @return the updated message
	 */
	@RequestMapping(value = "/updateMessage", method = RequestMethod.PUT)
	public ResponseEntity<Message> updateMessage(@RequestBody Message message) {
		messageService.updateMessage(message);
		
		return new ResponseEntity<>(message, HttpStatus.OK);
	}
}