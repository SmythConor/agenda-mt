package co.agendaapp.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;
import static org.springframework.web.bind.annotation.RequestMethod.DELETE;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

import co.agendaapp.model.User;
import co.agendaapp.model.ToDoListEntry;
import co.agendaapp.service.ToDoListService;
import co.agendaapp.exception.AppLayerException;

@Controller
@RequestMapping(value = "/toDoList")
public class ToDoListController {
	final Logger logger = LoggerFactory.getLogger(ToDoListController.class);
	
	@Autowired
	private ToDoListService toDoListService;
	
	/**
	 * Controller function to get to-do list entries for a group
	 * @param groupId the id of the group
	 * @return list of ToDoListEntry
	 */
	@RequestMapping(value = "/getEntries/{groupId}", method = GET)
	public ResponseEntity<List<ToDoListEntry>> getEntries(@PathVariable Integer groupId) {
		List<ToDoListEntry> toDoListEntries = toDoListService.getEntries(groupId);
		
		return new ResponseEntity<>(toDoListEntries, HttpStatus.OK);
	}
	
	/**
	 * Controller function to create to do list entries
	 * @param toDoListEntry the to do list entry to create
	 * @return the created todoListEntry
	 */
	@RequestMapping(value = "/createEntry", method = POST)
	public ResponseEntity<ToDoListEntry> createEntry(@RequestBody ToDoListEntry toDoListEntry) {
		toDoListService.createEntry(toDoListEntry);
		
		return new ResponseEntity<>(toDoListEntry, HttpStatus.CREATED);
	}
	
	/**
	 * Controller function to update a to do list entry
	 * @param toDoListEntry the entry to update
	 * @return the updated toDoList entry
	 */
	@RequestMapping(value = "/updateEntry", method = PUT)
	public ResponseEntity<ToDoListEntry> updateEntry(@RequestBody ToDoListEntry toDoListEntry) 
			throws AppLayerException {
		toDoListService.updateEntry(toDoListEntry);
		
		return new ResponseEntity<>(toDoListEntry, HttpStatus.OK);
	}
	
	/**
	 * Controller function to delete a to do list entry
	 * @param entryId the id of the entry to delete
	 * @return the deleted toDoList entry
	 */
	@RequestMapping(value = "/deleteEntry/{entryId}", method = DELETE)
	public ResponseEntity<String> deleteEntry(@PathVariable Integer entryId) 
			throws AppLayerException {
		toDoListService.deleteEntry(entryId);
		
		String success = "success";
		
		try {
			success = new ObjectMapper().writeValueAsString("success");
		} catch(JsonProcessingException e) {
			logger.error(e.getMessage());
		}
		
		return new ResponseEntity<>(success, HttpStatus.OK);
	}
}
