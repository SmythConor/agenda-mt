package co.agendaapp.model;

import java.io.Serializable;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Message implements Serializable {
	
	private static final long serialVersionUID = -2634717954998356770L;
	
	private Integer messageId;
	private Integer senderId;
	private Integer groupId;
	private String messageBody;
	private DateTime sentTimestamp;
	private DateTime deliveredTimestamp;
	
	public Integer getMessageId() {
		return messageId;
	}
	public void setMessageId(Integer messageId) {
		this.messageId = messageId;
	}
	public Integer getSenderId() {
		return senderId;
	}
	public void setSenderId(Integer senderId) {
		this.senderId = senderId;
	}
	public Integer getGroupId() {
		return groupId;
	}
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	public String getMessageBody() {
		return messageBody;
	}
	public void setMessageBody(String messageBody) {
		this.messageBody = messageBody;
	}
	public DateTime getSentTimestamp() {
		return sentTimestamp;
	}
	public void setSentTimestamp(DateTime sentTimestamp) {
		this.sentTimestamp = sentTimestamp;
	}
	public DateTime getDeliveredTimestamp() {
		return deliveredTimestamp;
	}
	public void setDeliveredTimestamp(DateTime deliveredTimestamp) {
		this.deliveredTimestamp = deliveredTimestamp;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Message [");
		if(messageId != null) {
			builder.append("messageId=");
			builder.append(messageId);
			builder.append(", ");
		}
		if(senderId != null) {
			builder.append("senderId=");
			builder.append(senderId);
			builder.append(", ");
		}
		if(groupId != null) {
			builder.append("groupId=");
			builder.append(groupId);
			builder.append(", ");
		}
		if(messageBody != null) {
			builder.append("messageBody=");
			builder.append(messageBody);
			builder.append(", ");
		}
		if(sentTimestamp != null) {
			builder.append("sentTimestamp=");
			builder.append(sentTimestamp);
			builder.append(", ");
		}
		if(deliveredTimestamp != null) {
			builder.append("deliveredTimestamp=");
			builder.append(deliveredTimestamp);
		}
		builder.append("]");
		return builder.toString();
	}
	
}
