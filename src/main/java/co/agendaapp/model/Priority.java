package co.agendaapp.model;

public enum Priority {
	HIGH, MEDIUM, LOW
}
